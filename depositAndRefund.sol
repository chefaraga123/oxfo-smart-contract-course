pragma solidity >=0.7.0 <0.9.0;

contract DepositAndRefund {
    
    mapping(address => uint256) balance;
    
    
    function getBalance(address _party)
        public
        view
        returns (uint256)
    {
        require(msg.sender == _party, 
        "only view own balance");
        
        return balance[_party];
    }
    
    
    function deposit(uint256 _deposit) 
        public 
        payable 
    {   
        require(msg.value == _deposit);
        
        balance[msg.sender] += _deposit;
    }
    
    function withdraw(uint256 _amount)
        public
    {
        address _addr = msg.sender;
        
        address payable addr = payable(_addr);
        
        balance[addr] -= _amount; 
        
        addr.transfer(_amount);
    }
    
}