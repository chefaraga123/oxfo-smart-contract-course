pragma solidity >=0.7.0 <0.9.0;

contract CryptoDogs {

    struct CryptoDog {
        uint256 dogDna;
        string dogName;
        uint256 birthTime;        
    }
    
    mapping (address => CryptoDog) public dogOwner;
    
    mapping (address => bool) public dogOwnership;
    
    mapping (uint => bool) public dnaUsed;
    
    CryptoDog[] public dogs;
    
    modifier dogOwned{
        require(dogOwnership[msg.sender] == false);
        _;
    }
    
    event dogCreated(string indexed dogName, uint indexed dogDna);
    

    function createCryptoDog(string memory _dogName, uint _dna) public {
        
        require(dnaUsed[_dna] == false);
        
        CryptoDog memory dog = CryptoDog(_dna , _dogName, block.timestamp);
        
        dnaUsed[_dna] = true;
        
        dogs.push(dog);
        
        dogOwner[msg.sender] = dog;
        
        dogOwnership[msg.sender] = true;
    
        emit dogCreated(_dogName, _dna);
        
    }
    
    
}



